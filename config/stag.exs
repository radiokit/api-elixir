use Mix.Config

config :radiokit_api,
  vault_base_url: "https://radiokit-vault-stag.herokuapp.com",
  plumber_base_url: "https://radiokit-plumber-stag.herokuapp.com"
