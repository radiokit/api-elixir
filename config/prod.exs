use Mix.Config

config :radiokit_api,
  vault_base_url: "https://radiokit-vault-prod.herokuapp.com",
  plumber_base_url: "https://radiokit-plumber-prod.herokuapp.com"
